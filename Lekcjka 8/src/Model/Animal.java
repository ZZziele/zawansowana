package Model;

public class Animal {

    // pola

    public int wiek;
    public String imie;
    public String rasa;

    // konstruktory

    public Animal(){

    }

    public Animal(int age, String name, String rasa){
        this.wiek = age ;
        this.imie = name;
        this.rasa = rasa;
    }

    public Animal(int age, String name){
        this.wiek = age ;
        this.imie = name;

    }

    public void show(){
        System.out.println("Wiek: " + wiek);
        System.out.println("Imie: " + imie);
        System.out.println("Rasa: " + rasa);
    }
}
