package Kalkulator;

public class Calculator {
    public double doCalculation(double firstNumber, double secondNumber,char operation){
        double rezultat;
        switch (operation){
            case '+':
                rezultat = add(firstNumber,secondNumber);
                break;
            case '-':
                rezultat = subtract(firstNumber,secondNumber);
                break;
            case '*':
                rezultat = multiplication(firstNumber,secondNumber);
                break;
            case '/':
                rezultat = division(firstNumber,secondNumber);
                break;
            default:
                rezultat = 0;
                break;

         }
        return rezultat;
    }
    private double add(double firstNumber,double secondNumber){
        return firstNumber + secondNumber;
    }
    private double subtract(double firstNumber,double secondNumber){
        return firstNumber - secondNumber;
    }

    private double multiplication(double firstNumber,double secondNumber){
        return firstNumber * secondNumber;
    }

    private double division(double firstNumber,double secondNumber){
        return firstNumber / secondNumber;
    }

}
