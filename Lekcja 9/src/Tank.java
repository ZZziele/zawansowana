public class Tank {

    private double capacity;

    public Tank(double capacity) {
        this.capacity = capacity;
    }

    public double getCapacity() {
        return capacity;
    }



    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }
}
