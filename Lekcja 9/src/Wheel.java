public class Wheel {

    public Wheel(int width) {
        this.width = width;
    }

    private int width;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
