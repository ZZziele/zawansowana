package zadanie20;

import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("podaj do jakiej liczby chce miec przedział");
        int przedzial = scanner.nextInt();

        gra(przedzial);
    }

    public static void gra (int przedzial){
        Scanner scanner = new Scanner(System.in);
        Random random  = new Random();
        int liczba = random.nextInt(przedzial+1);

        System.out.println("Podaj liczbe z przedziału 0  - " + przedzial);

        int strzal;
        do{
            strzal = scanner.nextInt();
            if(strzal == liczba){
                System.out.println("Bingo !");
                break;
            } else if (strzal < liczba && strzal >= 0)  System.out.println(" za malo");
            else if(strzal > liczba && strzal <= 100)   System.out.println(" za dużo");
            else System.out.println(" poza zakresem ");
        }while(true);
    }
}
