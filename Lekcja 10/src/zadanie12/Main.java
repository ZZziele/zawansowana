package zadanie12;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj jakiś ciag znaków");
        String napis = scanner.nextLine();
        char znak = ' ';
        int licznik = 0;
        for(int i = 0; i<napis.length(); i++){
            if(znak == napis.charAt(i)){
                licznik++;
            }
        }
        System.out.println("liczba spacji: " + licznik);
        System.out.println("liczba znaków: " + napis.length() );
        System.out.println("Procent: " + licznik *100.0d/napis.length());

    }
}
