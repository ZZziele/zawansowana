import javax.sound.midi.Soundbank;
import java.util.Scanner;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

       //String
        /*System.out.println("Podaj imie: ");
        Scanner scanner1 = new Scanner(System.in);
        String imie = scanner1.next();

        System.out.println("Podaj nazwisko: ");
        Scanner scanner2 = new Scanner(System.in);
        String nazwisko = scanner2.next();

        System.out.println("Nazywasz sie [" + imie +"] i masz na nazwisko ["+ nazwisko + "] " );

        System.out.println("Długośc twojego imienia to : " + imie.length());

        String zadanie_4 = "Veni, Vidi, Vici";
        System.out.println(zadanie_4.substring(6,10));
        String zadanie_5 = "si vis pacem para bellum";
        System.out.println(zadanie_5.toUpperCase());*/

        //IF

        /*System.out.println("podaj liczbe calkowita wiek");      //////////////////////////
        Scanner scannerif = new Scanner(System.in);
        int wiek = scannerif.nextInt();

        // 0 nie jest ani dodatnie ani ujemne
        if(wiek<0){
            System.out.println("liczba ujemna");
        } else if (wiek >0) {
            System.out.println("liczba dodatnia");
        }
        // przyjmuje że wiek może być tylko powyżej 0
        if(wiek <19){
            System.out.println("jesteś młody ");
        }else if( wiek < 66 ){
            System.out.println("jesteś w średnim wieku ");
        }else {
            System.out.println("jesteś stary");
        }

        if(wiek>=39 && wiek <=55 ){
            System.out.println("jesteś w przedziale wiekowym 39 do 55");
        }*/                     ////////////////////////////////////////

        // Petla for

     /*   for(int i =0 ;i<31;i++){                      ////////////////////////////////
            System.out.println(i);
        }

        for(int i =3 ;i<36;i++){
            if(i%2==1){
                continue;
            }
            System.out.print(i+",");
        }
        System.out.println("");
        System.out.println("");
        for (int i=20;i>=-10;i--){
            if(i%2==0){
                continue;
            }
            System.out.println(i);
        }*/                             ////////////////////////////////

        /*for (int i=0;i<=50;i++){
            if(i%2==1){
                continue;
            }
            if(i%8==0){
                System.out.println(i);
            }
        }*/



        // kwadrat
        /*System.out.println("podaj długośc boku");
        Scanner scannerif = new Scanner(System.in);
        int bok = scannerif.nextInt();

        for(int i=0 ;i<bok;i++){
            for(int j=0;j<=bok;j++){
                System.out.print("*");
            }
            System.out.println("");
        }


         // trujkąt
        for(int i=0 ;i<bok;i++){
            for(int j=0;j<=i;j++){
                System.out.print("*");
            }
            System.out.println("");
        }*/

        // piramida
        /*int bok  = 5 ;
        for(int i=0 ;i<bok;i++){

            for(int j=bok-i;j>=0;j--){
                System.out.print(" ");
            }
            for(int j=0;j<=(i*2);j++){
                System.out.print("*");
            }
            System.out.println("");

        }*/


        //Petla while
        //zadanie 1
        /*int i=15;
        while(i<=75){
            System.out.println(i);
            i++;
        }*/

        //zadanie 2

        /*System.out.println("podaj liczbe z zakresu 1-10");
        Scanner scanner2 = new Scanner(System.in);
        int liczba = scanner2.nextInt();
        while(liczba<1 || liczba > 10){
            System.out.println("Jesteś poza zakresem podaj liczbe jescze raz ");
             liczba = scanner2.nextInt();
        }*/

        // zadanie 3 (nie wiem czy w zadaniu n powiino być ustawiomne)
        /*int x =5 , y =10;
        for(int n =10 ;n>=1 ;n--){
            System.out.println(x+y);
        }*/

        // zadanie 4
        /*int wynik=0;
        int i=0;
        while (wynik<99){
            if(i%3==0){
                wynik=wynik+i;
            }
            System.out.println();
            i++;
        }
        System.out.println("wynik" + wynik + "  ostatnia dodana liczba to: " + (i-1));*/


        //zadanie 5

        /*Random liczba = new Random();
        int los = liczba.nextInt(10);
        System.out.println("Podaj liczbe z zakresu 1 -10 ");
        Scanner scanner2 = new Scanner(System.in);
        int liczba2 = scanner2.nextInt();
        while (liczba2 != los ){
            System.out.println("Pudło prubuj dalej :) ");
             liczba2 = scanner2.nextInt();
        }
        System.out.println("Gratulacje losowa liczba to :  " + los  );*/




        // Petla Do while


        //zadanie 1
       /* int i=0;
        do {
            System.out.println(i);
            i++;
        }while(i<=10);*/


        //zadanie 2
        // w tekście zadanie jest zamien z petli for na petle for wiec troche bez sensu. Wiec zamieniam na do while
        /*int i=1 , wartosc =10;
        do {
            wartosc=wartosc*3;
            System.out.println(wartosc);
            i++;
        }while(i<=10);*/

        //zadanie 3

       /* float cena =6.50f, wynik;
        int litry;

        System.out.println("podaj ile chcesz zatankować: ");
        Scanner scanner2 = new Scanner(System.in);
        litry = scanner2.nextInt();
        int i=0;
        do{
            i++;
            wynik = i*cena;
            System.out.println("litry: "+ i);
            System.out.println("Do zaplaty: "+wynik);

        }while(i<litry);*/




    }
}